﻿CREATE DATABASE proyectoipc2;
use proyectoipc2;

CREATE TABLE rol (
cod_rol INTEGER NOT NULL PRIMARY KEY, 
tipo_rol VARCHAR(30) NOT NULL);

CREATE TABLE usuario (
cod_usuario INTEGER NOT NULL PRIMARY KEY, 
nombre VARCHAR(45) NOT NULL, 
contrasena VARCHAR(45) NOT NULL, 
cod_rol INTEGER NOT NULL,  
FOREIGN KEY (cod_rol) REFERENCES rol(cod_rol));

CREATE TABLE recorrido (
cod_recorrido INTEGER NOT NULL PRIMARY KEY, 
titulo VARCHAR(40) NOT NULL, 
fecha_inicio DATE NOT NULL, 
fecha_fin DATE NOT NULL,
cod_usuario INTEGER NOT NULL,
FOREIGN KEY (cod_usuario) REFERENCES usuario(cod_usuario));

CREATE TABLE servicio (
cod_servicio INTEGER NOT NULL PRIMARY KEY, 
tipo_servicio VARCHAR(30) NOT NULL);

CREATE TABLE empresa (
cod_empresa INTEGER NOT NULL PRIMARY KEY, 
nombre VARCHAR(30) NOT NULL, 
tipo_empresa VARCHAR(30) NOT NULL, 
fotografia VARCHAR(100) NOT NULL, 
direccion VARCHAR(50)NOT NULL, 
telefono INTEGER NOT NULL, 
correo VARCHAR(30) NOT NULL, 
detalle VARCHAR(40) NOT NULL, 
tarifa FLOAT NOT NULL,
horario DATE NOT NULL, 
especialidad VARCHAR(40), 
cod_servicio INTEGER NOT NULL,
FOREIGN KEY (cod_servicio) REFERENCES servicio(cod_servicio));

CREATE TABLE solicitud (
correlativo INTEGER NOT NULL PRIMARY KEY, 
monto FLOAT NOT NULL, 
estado VARCHAR(30) NOT NULL,
cod_empresa INTEGER NOT NULL,
cod_usuario INTEGER NOT NULL,
FOREIGN KEY (cod_empresa) REFERENCES empresa(cod_empresa),
FOREIGN KEY (cod_usuario) REFERENCES usuario(cod_usuario));

CREATE TABLE recibo (
cod_recibo INTEGER NOT NULL PRIMARY KEY, 
pagoInscripcion FLOAT NOT NULL, 
correlativo INTEGER NOT NULL, 
FOREIGN KEY (correlativo) REFERENCES solicitud(correlativo));

CREATE TABLE sitioTuristico (
codigo INTEGER NOT NULL PRIMARY KEY, 
nombre VARCHAR(30) NOT NULL,
imagen VARCHAR(100) NOT NULL,
region VARCHAR(30) NOT NULL,
cod_empresa INTEGER NOT NULL,
FOREIGN KEY (cod_empresa) REFERENCES empresa(cod_empresa));

CREATE TABLE EVALUACION (
cod_evaluacion INTEGER NOT NULL PRIMARY KEY, 
estado VARCHAR(30) NOT NULL, 
detalle VARCHAR(100) NOT NULL, 
cod_empresa INTEGER NOT NULL, 
cod_recorrido INTEGER NOT NULL,  
FOREIGN KEY (cod_empresa) REFERENCES empresa(cod_empresa),
FOREIGN KEY (cod_recorrido) REFERENCES recorrido(cod_recorrido));

